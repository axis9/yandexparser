# coding: utf-8


# 85.195.82.34:3131 - check socks5

require_relative 'yandex_downloader.rb'
require_relative 'sqlite.rb'

# начальное количество нитей
START_THREADS = $*[0].to_i
MAX_TRY = 5
start_thread_added = false
# нити скачки
loader_threads = []
# флаг добавления нити
threads_need_add = nil

# контроллер для обращения к базе
control_db = LoaderController.new
# control_db.block_proxy 40
# exit
# обнуляем временные таблицы
control_db.start_clear
# exit
unless $*[0]
	puts 'Need first argument count threads!'
	exit
end
$next_thread_id = 0
def get_next_thread_id
	$next_thread_id+=1
	$next_thread_id
end

def add_thread(control_db)
	return Thread.new do
		begin
			Thread.current[:status] = "Starting..."
			Thread.current[:id] = get_next_thread_id
			# основная работа нитей/потоков
			# хватаем не занятого юзера
			Thread.current[:status] = "Get user"
			user = control_db.get_user
			# если юзеров нету - убиваем нить
			if user.nil?
				puts "Not have users. Thread killed"
				Thread.current[:status] = "No users"
				Thread.kill(Thredad.current)
			end
					
			# создаем юзера яндекса
			Thread.current[:status] = "Create account"
			account = YandexAcc.new(user['username'], user['password'])
			# локальные свойства нити
			Thread.current[:username] = user['username']
			# сколько скачано
			Thread.current[:downloaded] = 0
			# флаг - нужно обновить прокси
			Thread.current[:need_proxy] = true
			Thread.current[:proxy] = nil
			Thread.current[:proxy_id] = nil
			Thread.current[:died] = false
			Thread.current[:user_id] = user['id']
			loop do
				# p 'Start main thread loop'
				# если требуется обновить прокси
				Thread.current[:status] = "iteration start"
				if Thread.current[:need_proxy]
					Thread.current[:status] = "Updating proxy"
					# запускаем бесконечный цикл по поиску проксей
					# есть вероятность что в базе проксей не будет и нужно будет ждать
					loop do
						# хватам если надо не занятую проксю, которую юзал только этот аккаунт или новую
						Thread.current[:status] = "Try get proxy"
						user_proxy = control_db.get_proxy user['id']
						# если свободная прокси есть в базе и получена
						unless user_proxy.nil?
							# вписываем ее в аккаунт
							account.proxy = user_proxy['proxy']
							Thread.current[:proxy] = user_proxy['proxy']
							Thread.current[:proxy_id] = user_proxy['id']
							unless user_proxy['type'].eql? 'http'
								account.proxy_type = case user_proxy['type']
									when 'http' then Curl::CURLPROXY_HTTP
									when 'socks4' then Curl::CURLPROXY_SOCKS4
									when 'socks5' then Curl::CURLPROXY_SOCKS5
									else Curl::CURLPROXY_HTTP
								end
							end
							account.proxy_pw = user_proxy['username'] + ":" + user_proxy['password'] unless user_proxy['username'].nil? or user_proxy['username'].empty?
							# обрываем бесконечный цикл по поиску проксей
							Thread.current[:need_proxy] = false
							break
						else
							Thread.current[:status] = "Wait proxy"
							# если прокси нету, то ждем час
							sleep 3600
						end
					end
				end

				# raise "Boom!" if rand(3) == 2

				Thread.current[:status] = "Try load page"
				# MAX_TRY попыток скачки страницы через прокси
				page_loaded_flag = false
				MAX_TRY.times do |time_iteration|
				temp_result = account.test
				unless temp_result.nil?
					test_result = temp_result.body_str.scan(/\d+\.\d+\.\d+\.\d+/)[0]
					puts "%s => Test result:%s | Yandex.User: %s | Proxy: %s" % [Thread.current[:id], test_result, Thread.current[:username], Thread.current[:proxy]]
					# время между скачками
					Thread.current[:downloaded] += 1
					break
				else
					puts "Proxy %s died" % Thread.current[:proxy]
					Thread.current[:need_proxy] = true
				end
			end

			unless page_loaded_flag
				# Thread.current[:need_proxy] = true
				# control_db.ban_proxy Thread.current[:proxy_id]
				# Thread.current[:proxy] = nil
				# Thread.current[:proxy_id] = nil
			end

			Thread.current[:status] = "End iteration... Sleep"
			sleep(10+rand(4))
			# sleep 1
		end
		rescue Exception => msg
			puts "Thread #{Thread.current[:id]} died"
			Thread.current[:died] = true
			Thread.current[:status] = 'Died'
			control_db.delete_current_proxy Thread.current[:proxy_id]
			control_db.delete_current_user Thread.current[:user_id]
			Thread.exit Thread.current
		end
	end
end

# стартовый запуск управляющего цикла
puts "Control started!"
loop do
	# проверка на добавленные стартовых нитей
	unless start_thread_added
		# надо добавить нитей
		threads_need_add = START_THREADS
		# меняем флаг чтобы добавления больше не было
		start_thread_added = true
	end

	# проверка на добавление нитей
	unless threads_need_add.nil?
		# Добавяление потоков/нитей
		threads_need_add.times do |iteration|
			# добавляем нити в массив
			loader_threads << add_thread(control_db)
			# время между запусками нитей рандомное от 7 до 12 секунд
			sleep(7+rand(5))
		end
		# обнуляем количество нитей
		threads_need_add = nil
	end

	# время между проверкой управляющих команд
	sleep 5

	# Показать текущие статусы нитей
	puts "###############################"

	loader_threads.each do |thread|
		if thread[:died]
			# Thread.kill thread
			puts "Rewrite thread #{thread[:username]}" 
			# exit
			thread = add_thread(control_db)
		else
			puts "%s => %s => %s" % [thread[:username], thread[:status], thread[:died]]
		end
	end	
end