require 'sqlite3'

class DB
	attr_reader :db_sqlite

	def initialize
		@db_sqlite = SQLite3::Database.new 'thread_base.db'
		@db_sqlite.results_as_hash = true
	end

	def insert(table, args)
		@db_sqlite.execute("insert into #{table} values (?"+(',?' *(args.size-1))+")",args)
	end

	def get_max(table)
		@db_sqlite.get_first_row("select max(id)+1 as cnt from #{table}")['cnt']
	end

	def raw(sql)
		@db_sqlite.execute(sql)
	end

	def select(table, where=nil)
		sql = "select * from #{table}"
		sql << " where #{where}" unless where.nil?
		@db_sqlite.execute sql
	end

	def update(table, where=nil, args)
		sql = "update #{table} set "
		sql << args.map do |key, value| 
			if value.instance_of? String
				"%s='%s'" % [key, value]
			else
				"%s=%s" % [key, value]
			end
		end.join(', ') if args.size > 0
		sql << " where #{where}" unless where.nil?
		@db_sqlite.execute sql
	end

	def select(table, where=nil)
		sql = "select * from #{table}"
		sql << " where #{where}" unless where.nil?
		@db_sqlite.execute sql
	end

	def delete(table, where=nil)
		sql = "delete from #{table}"
		sql << " where #{where}" unless where.nil?
		@db_sqlite.execute sql
	end

	def raw_one(sql)
		@db_sqlite.get_first_row(sql)
	end
end

class LoaderController
	COM_ADDTHREADS = 1
	COM_REMOVETHREADS = 2

	MAX_FAIL_PROXY = 3

	attr_reader :db

	def initialize
		@db = DB.new
	end

	def add_command(type, args)
		@db.insert('commands', [0, type, args])
	end

	def get_proxy(user_id)
		proxy = @db.raw_one("select p.id, p.type, p.proxy, p.username, p.password from proxy p where p.id not in (select cp.proxy_id from current_proxy cp) and p.id not in (select up.proxy_id from used_proxy up where up.user_id!=#{user_id})")
		return nil if proxy.nil?
		@db.insert('current_proxy', [@db.get_max('current_proxy'), proxy['id'], user_id, Time.now.to_i])
		total = @db.raw_one("select count(1) cnt from used_proxy where user_id=#{user_id}")['cnt']
		@db.insert('used_proxy', [@db.get_max('current_proxy'), user_id, proxy['id'], Time.now.to_i]) if total.to_i == 0
		proxy
	end

	def delete_current_proxy(proxy_id)
		puts "Proxy with id #{proxy_id} deleted"
		@db.delete('current_proxy', "proxy_id=#{proxy_id}")
		true
	end

	def delete_current_user(user_id)
		puts "User with id #{user_id} deleted"
		@db.delete('current_users', "user_id=#{user_id}")
		true
	end

	def get_user
		user = @db.raw_one('select u.id, u.username, u.password from users u where u.id not in (select cu.user_id from current_users cu)')
		return nil if user.nil?
		@db.insert('current_users', [@db.get_max('current_users'), user['id'], Time.now.to_i])
		user
	end

	def start_clear
		@db.raw 'delete from current_users'
		@db.raw 'delete from current_proxy'
	end

	def block_proxy(proxy_id)
		proxy = @db.select('proxy', "id=#{proxy_id}")[0]
		if proxy['fail_connection_count'].to_i >= MAX_FAIL_PROXY
			@db.update 'proxy', "id=#{proxy_id}", { enabled:0 }
		end
	end
end